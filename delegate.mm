#include <QFileInfo>
#include <QImage>
#include <QtDebug>
#include <QStandardPaths>
#include <QDateTime>

#include "delegate.h"
#include "truedepthcamera.h"


#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAudioSession.h>

#pragma clang diagnostic ignored "-Wungarded-availability"
#pragma clang diagnostic ignored "-Wpartial-availability"

static QImage fromCGImage(CGImageRef image, QImage::Format format);
static QImage fromUIImage(UIImage *image, QImage::Format format);

@implementation CaptureDelegate

- (void)captureOutput:(AVCapturePhotoOutput *)output
  didFinishProcessingPhoto:(AVCapturePhoto *)photo
  error:(nullable NSError *)error
{
//  NSData *imageData = [photo fileDataRepresentation];
	AVDepthData *depthData = photo.depthData;

	QImage image = fromCGImage(photo.CGImageRepresentation, QImage::Format_RGB32);
	QString dataDir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
	QString filename = dataDir + "/image.png";
	bool saved = false;
	//saved = image.save(filename);
	if (saved) {
		TrueDepthCamera::instance()->capturedImage("file://"+filename);
	}
	qDebug() << Q_FUNC_INFO << "image" << image << saved;

	QImage depthImage;

	if (depthData) {
		size_t width = CVPixelBufferGetWidth(depthData.depthDataMap);
		size_t height = CVPixelBufferGetHeight(depthData.depthDataMap);
		qDebug() << Q_FUNC_INFO << "depthImage " << width << height << depthData << depthData.depthDataType;

		AVDepthData *floatDepthData = depthData;
		if (depthData.depthDataType != kCVPixelFormatType_DepthFloat32) {
			floatDepthData = [depthData depthDataByConvertingToDepthDataType: kCVPixelFormatType_DepthFloat32];
		}

		OSType type = CVPixelBufferGetPixelFormatType( floatDepthData.depthDataMap);
		qDebug() << "type" << type << (kCVPixelFormatType_DepthFloat32 == type);

		CVPixelBufferLockBaseAddress(floatDepthData.depthDataMap, kCVPixelBufferLock_ReadOnly);
		float *floatBuffer = (float*) CVPixelBufferGetBaseAddress(floatDepthData.depthDataMap);
		qDebug() << "floatBuffer" << floatBuffer;
		float minPixel = 1.0;
		float maxPixel = 0.0;

		for (size_t y=0; y<height; y++) {
			for (size_t x=0; x<width; x++) {
				float pixel = floatBuffer[y * width + x];
				minPixel = fmin(pixel, minPixel);
				maxPixel = fmax(pixel, maxPixel);
			 }
		}

		QMap<int, int> vs;
		float range = maxPixel - minPixel;
		qDebug() << Q_FUNC_INFO << "range" << minPixel << range << maxPixel;
		depthImage = QImage(width, height, QImage::Format_RGB32);
		for (size_t y=0; y<height; y++) {
			for (size_t x=0; x<width; x++) {
				float normPixel = 1- (( floatBuffer[y * width + x] - minPixel) / range);
				int v = 255 * normPixel;
				depthImage.setPixel(x,y, qRgb(v,v,v));
			}
		}

		CVPixelBufferUnlockBaseAddress(floatDepthData.depthDataMap, kCVPixelBufferLock_ReadOnly);
		filename = QString("%1/depthImage-%2.jpg").arg(dataDir).arg(QDateTime::currentSecsSinceEpoch());
		bool saved = depthImage.save(filename);
		QFileInfo fi(filename);
		qDebug() << Q_FUNC_INFO << "saved" << saved <<  fi.absoluteFilePath();
		if (saved) {
			TrueDepthCamera::instance()->capturedDepth("file://"+filename);
		}

	} else {
		qDebug() << Q_FUNC_INFO << "no depthData acquired";
	}

	if (error != nil) {
		qDebug() << Q_FUNC_INFO << error;
	} else {
		qDebug() << Q_FUNC_INFO << "no error";
	}



}


@end

void TrueDepthCamera::capture() {
	AVCaptureDevice *device = [AVCaptureDevice
								defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInTrueDepthCamera
								mediaType: AVMediaTypeDepthData
								position:AVCaptureDevicePositionFront];

	if (device == nil) {
		qDebug() << "DEVICE_NOT_APPLICABLE"; // called on iPhone without TrueDepth camera
		return;
	}

	NSError* error = nil;
	AVCaptureSession *session = [[AVCaptureSession alloc] init];
	AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice: device  error: &error];

	if (error) {
		qDebug() << "FAIL: "+QString::fromNSString(error.localizedDescription);
		return;
	}


	[session beginConfiguration];
	[session addInput:input];

	AVCapturePhotoOutput *output = [[AVCapturePhotoOutput alloc] init];

	[session addOutput: output];
	session.sessionPreset = AVCaptureSessionPresetPhoto;
	[session commitConfiguration];

	AVCapturePhotoSettings *photoSettings = [AVCapturePhotoSettings photoSettings];

	CaptureDelegate *captureDelegate = [[CaptureDelegate alloc] init];

	/* these must be after the inputs and outputs are set up */
	output.depthDataDeliveryEnabled = YES;
	photoSettings.depthDataDeliveryEnabled = YES;
	/* but before the session is started */

	[session startRunning];
	[output capturePhotoWithSettings: photoSettings delegate:captureDelegate];


	if (error != nil) {
		qDebug() << "FAIL: "+QString::fromNSString(error.localizedDescription);
		return;
	}

	qDebug() << "CAPTURING";

}


static QImage fromUIImage(UIImage* image, QImage::Format format) {
	CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
	CGFloat width = image.size.width;
	CGFloat height = image.size.height;

	int orientation = [image imageOrientation];
	int degree = 0;

	switch (orientation) {
	case UIImageOrientationLeft:
		degree = -90;
		break;
	case UIImageOrientationDown: // Down
		degree = 180;
		break;
	case UIImageOrientationRight:
		degree = 90;
		break;
	}

	if (degree == 90 || degree == -90)  {
		CGFloat tmp = width;
		width = height;
		height = tmp;
	}

	QSize size(width,height);

	QImage result = QImage(size,format);
	int bitsPP = 8;

	CGContextRef contextRef = CGBitmapContextCreate(result.bits(),               // Pointer to  data
													width,                       // Width of bitmap
													height,                      // Height of bitmap
													bitsPP,                      // Bits per component
													result.bytesPerLine(),       // Bytes per row
													colorSpace,                  // Colorspace
													kCGImageAlphaNoneSkipFirst |
													kCGBitmapByteOrder32Little); // Bitmap info flags

	CGContextDrawImage(contextRef, CGRectMake(0, 0, width, height), image.CGImage);
	CGContextRelease(contextRef);

	if (degree != 0) {
		QTransform myTransform;
		myTransform.rotate(degree);
		result = result.transformed(myTransform,Qt::SmoothTransformation);
	}

	return result;
}

static QImage fromCGImage(CGImage* image, QImage::Format format) {
	UIImage* uiImage = [[UIImage alloc] initWithCGImage:image];
	return fromUIImage(uiImage, format);
}

