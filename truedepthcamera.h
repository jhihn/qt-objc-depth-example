#ifndef TRUEDEPTHCAMERA_H
#define TRUEDEPTHCAMERA_H

#include <QObject>


class TrueDepthCamera: public QObject {
	Q_OBJECT
	static TrueDepthCamera *_instance;
public:
	TrueDepthCamera(QObject *parent=nullptr);
	static TrueDepthCamera *instance();

public slots:
	void capture();

signals:
	void capturedImage(QString imageFilename);
	void capturedDepth(QString depthImageFilename);
};


#endif // TRUEDEPTHCAMERA_H
