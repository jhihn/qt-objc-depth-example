# qt-objc-depth-example

This is just a dummy test project for taking photos with in a combination of Objective-C and Qt. 

I did this because there are hardly any examples of using Objective-C to get depth data. 
I used Obj-C because Qt can't use Swift (which all the examples seem to be in)
I used Qt because that is what I normally use.

DISCLAIMER: I am not a Obj-C or Swift developer, nor am I familar with the Apple Libraries. 
But this works,

![](screenshot.jpg)