#include "truedepthcamera.h"
#include <QtDebug>

TrueDepthCamera *TrueDepthCamera::_instance = nullptr;

TrueDepthCamera::TrueDepthCamera(QObject*parent): QObject(parent){
	if (!_instance)
		_instance = this;
}

TrueDepthCamera* TrueDepthCamera::instance() {
	if (!_instance)
		new TrueDepthCamera();

	return _instance;
}

