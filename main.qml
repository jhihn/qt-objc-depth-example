import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.0
import com.truedepth 1.0

Window {
	visible: true
	width: 640
	height: 480
	title: qsTr("Hello World")

	TrueDepthCamera {
		id: camera
		onCapturedImage: {
			console.log("imageFilename:", imageFilename);
		}
		onCapturedDepth: {
			console.log("depthFilename:", depthImageFilename);
			image.source = depthImageFilename;
		}

	}

	Image {
		id: image
		anchors.fill: parent
		fillMode: Image.PreserveAspectFit
		mipmap: false


	}

	Button {
		text: "capture"
		onClicked: {
			camera.capture();
		}
	}


}
