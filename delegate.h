#ifndef DELEGATE_H
#define DELEGATE_H

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface CaptureDelegate:  NSObject< AVCapturePhotoCaptureDelegate >
@end


#endif // DELEGATE_H
